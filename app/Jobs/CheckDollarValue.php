<?php

namespace App\Jobs;

use App\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckDollarValue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $val = Payment::where('payment_date', today())
            ->whereNotNull('clp_usd')
            ->first();

        if (empty($val)) { // Request value to API
            $client = new \GuzzleHttp\Client();
            $resp = $client->get('https://mindicador.cl/api/dolar');
            $data = json_decode($resp->getBody());
            $this->payment->clp_usd = $data->serie[0]->valor;
        } else {
            $this->payment->clp_usd = $val->clp_usd;
        }
        $this->payment->save();
    }
}
