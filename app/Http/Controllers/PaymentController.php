<?php

namespace App\Http\Controllers;

use App\Events\PaymentCreated;
use App\Jobs\CheckDollarValue;
use App\Payment;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('client')) {
            return Payment::where('user_id', $request->client)->get();
        }
        return Payment::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment = new Payment();
        $payment->uuid = Str::uuid();
        $payment->payment_date = today();
        $payment->fill($request->all())->save();
        
        CheckDollarValue::dispatch($payment);
        
        event(new PaymentCreated($payment));
        
        return $payment;
    }
}
