<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Return the join_date attribute in simple date format.
     */
    public function getJoinDateAttribute($value)
    {
        return (new \Carbon\Carbon($value))->format('Y-m-d');
    }

    /**
     * Get the payments of the client.
     */
    public function payments()
    {
        return $this->hasMany('App\Payment', 'user_id');
    }
}
