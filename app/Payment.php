<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'expires_at', 'status', 'user_id',
    ];

    /**
     * Get the client that owns the payment.
     */
    public function client()
    {
        return $this->belongsTo('App\Client', 'user_id');
    }
}
