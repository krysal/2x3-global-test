<?php

use App\Payment;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Payment::create([
            "uuid" => Str::uuid(),
		    "payment_date" => "2017-01-01",
		    "expires_at" => "2019-01-01",
		    "status" => "pending",
		    "user_id" => 1,
		    "clp_usd" => 810,
        ]);

        Payment::create([
            "uuid" => Str::uuid(),
            "payment_date" => "2019-03-03",
            "expires_at" => "2020-03-30",
            "status" => "paid",
            "clp_usd" => 800,
            "user_id" => 1
        ]);

        Payment::create([
            "uuid" => Str::uuid(),
            "payment_date" => "2020-01-26",
            "expires_at" => "2020-02-26",
            "status" => "paid",
            "user_id" => 2,
            "clp_usd" => 820.01,
        ]);
    }
}
