<?php

use App\Client;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = Client::create([
            'name' => 'Krystle Salazar',
            'email' => 'admin@example.com'
        ]);

        $client = Client::create([
            'name' => 'Guillermo',
            'email' => 'guillermo@2x3.cl'
        ]);
        
        $client = Client::create([
            'name' => 'Eduardo',
            'email' => 'eduardo@2x3.cl'
        ]);

        $client->save();
    }
}
