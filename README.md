# Solución al Desafio 2x3 Global

Este proyecto representa mi solución al [desafío](/CHALLENGE.md) propuesto por 2x3 Global.

## Premisas

Para poder completar el desafío, se asumen las siguientes premisas de algunos detalles no incluídos en la descripción:

- En `status` se guarda simplemente el valor recibido, siempre que sea `'pending'` o `'paid'`.
- El campo `payment_date` se toma como la fecha en que se crea el pago (la hora del sistema cuando se recibe la solicitud), independientemente del `status` del mismo.
- En `clp_usd` se guarda el último valor arrojado por el [API](https://mindicador.cl/api/dolar) el día en que es consultado.

---

## Instrucciones de instalación

1. Clonar el proyecto
2. Ir a la carpeta del proyecto `cd 2x3-global-test` y ejecutar `composer install`
3. Establecer las variables para la base de datos _relacional_ en el archivo .env
4. Ejecutar `php artisan migrate:refresh --seed` para aplicar las migraciones y tener algunos registros de prueba
5. Establecer las variables para el envío de correos en el archivo .env (en mi caso usé [Mailhog](https://laravel.com/docs/6.x/homestead#configuring-mailhog)). Para el envío de correos ejecutar `php artisan queue:work --queue=mails`
6. Establecer las variables para el manejo de las colas de procesos en el archivo .env (en mi caso utilicé la conexión `database`). Para procesar los jobs ejecutar `php artisan queue:work`
7. Listo, solo queda probar los endpoint:
    - `GET /clients`
    - `GET /payments?client=2`
    - `POST /payments` con los parámetros `expires_at, status, user_id`


---

## TODO

Improvements suggested:

- [ ] Make validations of data received
    - [] `expires_at` > `payment_date` = CURRENT_DATE
    - [] `status` should exists in `['pending', 'paid']`
    - [] A payment should be related to an existing client
- [ ] Add automated tests
- [ ] Make endpoint to create a client
- [ ] Handle errors when consulting the API (in case when is down)

---

## Autora

Krystle Salazar - [Contacto](https://www.linkedin.com/in/krysal)